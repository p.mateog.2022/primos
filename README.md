# Repositoro plantilla: "Cálculo de números primos"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.etsit.urjc.es/cursoprogram/materiales/-/blob/main/practicas/ejercicios/README.md#primos), que incluye la fecha de entrega.

entrega:
[primo.py](https://gitlab.etsit.urjc.es/p.mateog.2022/primos/-/blob/main/primos.py)
